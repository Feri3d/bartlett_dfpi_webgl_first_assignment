const regl = require('regl')()
const glm = require('gl-matrix')
var mat4 = glm.mat4

//Loading Vertex Shader via external JS file
const strVertex = require("./shaderVertex.js")
//console.log("strVertex", strVertex)

//Loading Frag Shader via external JS file
const strFrag = require("./fragShader.js")
//console.log("strVertex", strVertex)

// Importing object into the scene using an external JS code
const loadObj = require('./utils/loadObj')

/**************************************************************************************************/
// Defining the Camera

var projectionMatrix = mat4.create()
var fov = 75 * Math.PI / 180
var aspect = window.innerWidth / window.innerHeight
mat4.perspective(projectionMatrix, fov, aspect, 0.01, 1000.0)
//Near = 0.01, Far = 1000.0

var viewMatrix = mat4.create()

//Receiving mouse position from the screen inside our browser
var mouseX = 0, mouseY = 0;

window.addEventListener('mousemove', e => {
    var x = event.clientX // get the mosue position from the event
    var y = event.clientY

    mouseX = map(x, 0, window.innerWidth, -25, 25)
    mouseY = -map(y, 0, window.innerHeight, -25, 25)
    //console.log(mouseX, mouseY);
});

window.addEventListener('mousemove', function (event) {  
})

//Mapping function to map the mouse position to camera position
function map (value, start, end, newStart, newEnd) {
  var percent = (value - start) / (end - start)
  if (percent < 0) {
    percent = 0
  }
  if (percent > 1) {
    percent = 1
  }
  var newValue = newStart + (newEnd - newStart) * percent
  return newValue
}
/**************************************************************************************************/
var drawCube;

//Loading object using the loadObj
loadObj('./assets/myObject.obj', function(obj) {
  //console.log('Model Loaded', obj)
  
  // Defining the attribute
  var attributes = {
    aPosition: regl.buffer(obj.positions),
    aUV: regl.buffer(obj.uvs)
  }
  
  //Draw call
  drawCube = regl({
    uniforms: {
      uTime: regl.prop("time"),
      uProjectionMatrix: regl.prop("projection"),
      uViewMatrix: regl.prop("view"),
      uTranslate: regl.prop("translate"),
      uColor: regl.prop("color")
    },
    vert: strVertex,
    frag: strFrag,
    attributes: attributes,
    count: obj.count
  })
})

function clear (){
    regl.clear({
        color: [0, 0, 0, 0],
    })
}
/**************************************************************************************************/
//Defining our model and its characteristic including the location of our model & Color ... (using the Vert & Frg shader)
var  currTime = 0;

function render(){
    currTime += 0.01

    mat4.lookAt(viewMatrix, [mouseX, mouseY, 25], [0,0,0], [0,1,0])
    //Eye = [mouseX, mouseY, 25];   Centre = [0,0,0];   Up = [0,1,0]

    clear()

    if (drawCube != undefined)  {
      
      //Defining number of objects
      var num = 10

      for(var i = 0; i<num; i++)  {
        for (var j = 0; j<num; j++)  {
          for(var k = 0; k<num; k++) {
            var obj = {
              time: currTime,
              projection: projectionMatrix,
              view: viewMatrix,
              translate: [ (i - (num/2)) * 1.5, (j- (num/2)) * 1.5, k * 1.5],
              color: [i/num, j/num, k/num]
            }
            drawCube(obj)
          } 
        }
      }
    }
   window.requestAnimationFrame(render)
}

render()