module.exports =

`
precision mediump float;
varying vec2 vUV;
varying float vNoise;

uniform vec3 uColor;

void main() {
    gl_FragColor = vec4(uColor, 1.0);
}`





